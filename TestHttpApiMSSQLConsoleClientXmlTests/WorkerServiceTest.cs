using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Api.Requests;
using Api.Requests.Types;
using Domain;
using Domain.Types;
using NUnit.Framework;
using TestHttpApiMSSQLConsoleClientXml.Mappings;
using TestHttpApiMSSQLConsoleClientXml.Services;
using TestHttpApiMSSQLConsoleClientXmlTests.Common;

namespace TestHttpApiMSSQLConsoleClientXmlTests
{
    [TestFixture]
    public class WorkerServiceTest : AbstractIntegrationTests
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _service = new WorkerService(Repository, new WorkerMapper(Mapper.Value));
        }

        private WorkerService _service;

        [Test]
        public async Task TestAdd()
        {
            var dto = new CreateRequest
            {
                Name = "Test",
                Type = WorkerTypeDto.Hourly,
                Tax = 27
            };

            var result = await _service.Create(dto);
            Assert.That(result, Is.Not.Null);

            Debug.Assert(result.Id != null, "result.Id != null");
            var entity = await Repository.ById(result.Id.Value);

            Assert.That(entity, Is.Not.Null);
            Assert.That(result.Name, Is.EqualTo(entity.Name));
            Assert.That(result.Tax, Is.EqualTo(entity.Tax));
            Assert.That(result.Salary, Is.EqualTo(entity.Salary));
            Assert.That((int) dto.Type, Is.EqualTo((int) entity.Type));
        }

        [Test]
        public async Task TestByMaxHourlyTax()
        {
            await Repository.Add(new WorkerEntity {Name = "Anon2", Type = WorkerType.Hourly, Tax = 20});
            var max = await Repository.Add(new WorkerEntity {Name = "Anon", Type = WorkerType.Hourly, Tax = 200});
            await Repository.Add(new WorkerEntity {Name = "Anon3", Type = WorkerType.Hourly, Tax = 5});

            var result = await _service.ByMaxHourlyTax();

            Assert.That(result.Id, Is.EqualTo(max.Id));
        }

        [Test]
        public async Task TestByName()
        {
            var testEntity = new WorkerEntity {Name = "tesName", Type = WorkerType.Hourly, Tax = 100500};
            var worker = await Repository.Add(testEntity);
            await Repository.Add(GetRandomWorker());
            await Repository.Add(GetRandomWorker());
            Assert.That(Db.WorkerEntities.Count(), Is.EqualTo(3));

            var byNameWorker = await _service.ByName(testEntity.Name);

            Assert.That(byNameWorker, Is.Not.Null);
            Assert.That(worker.Id, Is.EqualTo(byNameWorker.Id));
            Assert.That(testEntity.Name, Is.EqualTo(byNameWorker.Name));
            Assert.That(testEntity.Tax, Is.EqualTo(byNameWorker.Tax));
            Assert.That(testEntity.Salary, Is.EqualTo(byNameWorker.Salary));
        }

        [Test]
        public async Task TestGetPageDataSorting()
        {
            var fe = await Repository.Add(new WorkerEntity {Name = "Anon", Type = WorkerType.Monthly, Tax = 100500});
            var se = await Repository.Add(new WorkerEntity {Name = "Anon1", Type = WorkerType.Monthly, Tax = 10050});
            var te = await Repository.Add(new WorkerEntity {Name = "Bnon1", Type = WorkerType.Monthly, Tax = 10050});
            await Repository.Add(new WorkerEntity {Name = "Anon2", Type = WorkerType.Monthly, Tax = 1005});

            var page = await _service.GetPage(new PageRequest(0, 100));

            var first = page.Data.First();
            Assert.That(first.Id, Is.EqualTo(fe.Id));
            Assert.That(first.Name, Is.EqualTo(fe.Name));
            Assert.That(first.Tax, Is.EqualTo(fe.Tax));
            Assert.That(first.Salary, Is.EqualTo(fe.Salary));

            Assert.That(page.Data[1].Id, Is.EqualTo(se.Id));
            Assert.That(page.Data[2].Id, Is.EqualTo(te.Id));
        }

        [Test]
        public async Task TestGetPagePaging()
        {
            await Repository.Add(GetRandomWorker());
            await Repository.Add(GetRandomWorker());
            await Repository.Add(GetRandomWorker());
            await Repository.Add(GetRandomWorker());

            var page = await _service.GetPage(new PageRequest(0, 2));

            Assert.That(page.Total, Is.EqualTo(4));
            Assert.That(page.Data.Count(), Is.EqualTo(2));

            page = await _service.GetPage(new PageRequest(0, 100));

            Assert.That(page.Total, Is.EqualTo(4));
            Assert.That(page.Data.Count(), Is.EqualTo(4));

            page = await _service.GetPage(new PageRequest(50, 100));

            Assert.That(page.Total, Is.EqualTo(4));
            Assert.That(page.Data.Count(), Is.EqualTo(0));
        }

        [Test]
        public async Task TestTotalMonthSalary()
        {
            await Repository.Add(new WorkerEntity {Name = "Anon", Type = WorkerType.Monthly, Tax = 200});
            await Repository.Add(new WorkerEntity {Name = "Anon1", Type = WorkerType.Hourly, Tax = 20});

            var result = await _service.TotalMonthSalary();

            Assert.That(result.Total, Is.EqualTo(3528));
        }
    }
}