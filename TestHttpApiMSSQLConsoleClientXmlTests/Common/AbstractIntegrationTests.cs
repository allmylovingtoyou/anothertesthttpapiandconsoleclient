using System;
using AutoMapper;
using Db;
using Domain;
using Domain.Types;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using TestHttpApiMSSQLConsoleClientXml.Mappings.Base;
using TestHttpApiMSSQLConsoleClientXml.Repositories;

namespace TestHttpApiMSSQLConsoleClientXmlTests.Common
{
    [SetUpFixture]
    public abstract class AbstractIntegrationTests
    {
        protected ApplicationDbContext Db;
        protected Lazy<IMapper> Mapper;
        protected WorkerRepository Repository;

        [OneTimeSetUp]
        public void Init()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer("Server=RAB-04\\SQLEXPRESS1;Database=Tn_Test_Db_Test;Trusted_Connection=True;ConnectRetryCount=0;");

            var db = new ApplicationDbContext(options.Options);
            db.Database.EnsureDeleted();
            db.Database.Migrate();
            StoredProcedureCreate.Create(db);
            Db = db;
        }

        [SetUp]
        public virtual void Setup()
        {
            Repository = new WorkerRepository(Db);
            Mapper = new Lazy<IMapper>(GetAutoMapper());
        }

        [TearDown]
        public void Dispose()
        {
            Db.RemoveRange(Db.WorkerEntities);
            Db.SaveChanges();
        }

        protected WorkerEntity GetRandomWorker()
        {
            var rnd = new Random();
            return new WorkerEntity
            {
                Name = Guid.NewGuid().ToString().Substring(0, 20),
                Type = (WorkerType) rnd.Next(1, 2),
                Tax = rnd.Next(100, 500)
            };
        }

        private static IMapper GetAutoMapper()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            return mappingConfig.CreateMapper();
        }
    }
}