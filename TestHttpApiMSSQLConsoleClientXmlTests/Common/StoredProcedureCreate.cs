using Db;
using Microsoft.EntityFrameworkCore;

namespace TestHttpApiMSSQLConsoleClientXmlTests.Common
{
    public class StoredProcedureCreate
    {
        private const string AddWorker =
            "create PROCEDURE [dbo].[AddWorker] "
            + "@Name Varchar(50), @Type Int, @Tax Float, @Salary Float "
            + "AS BEGIN DECLARE @Id TABLE(id Int); "
            + "INSERT INTO WorkerEntities (Name, Type, Tax, Salary ) OUTPUT inserted.id into @id VALUES ( @Name, @Type, @Tax, @Salary )"
            + "RETURN (SELECT Id FROM @id) END";

        private const string ByName =
            "create PROCEDURE [dbo].[ByName] "
            + "@Name Varchar(50) "
            + "AS Select * FROM WorkerEntities WHERE Name = @Name";

        public static void Create(ApplicationDbContext db)
        {
            db.Database.ExecuteSqlCommand(AddWorker);
            db.Database.ExecuteSqlCommand(ByName);
        }
    }
}