using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Types;
using NUnit.Framework;
using TestHttpApiMSSQLConsoleClientXmlTests.Common;

namespace TestHttpApiMSSQLConsoleClientXmlTests
{
    [TestFixture]
    public class WorkerRepositoryTest : AbstractIntegrationTests
    {
        [Test]
        public async Task TestAdd()
        {
            var testEntity = new WorkerEntity {Name = "tesName", Type = WorkerType.Hourly, Tax = 100500};
            var worker = await Repository.Add(testEntity);
            Assert.That(worker, Is.Not.Null);
            Assert.That(worker.Id, Is.Not.EqualTo(0));
            Assert.That(worker.Name, Is.EqualTo(testEntity.Name));
            Assert.That(worker.Type, Is.EqualTo(testEntity.Type));
            Assert.That(worker.Tax, Is.EqualTo(testEntity.Tax));
            Assert.That(worker.Salary, Is.EqualTo(testEntity.Salary));
        }

        [Test]
        public async Task TestByName()
        {
            var testEntity = new WorkerEntity {Name = "tesName", Type = WorkerType.Hourly, Tax = 100500};
            var worker = await Repository.Add(testEntity);
            await Repository.Add(GetRandomWorker());
            await Repository.Add(GetRandomWorker());

            Assert.That(Db.WorkerEntities.Count(), Is.EqualTo(3));

            var byNameWorker = await Repository.ByName(testEntity.Name);

            Assert.That(byNameWorker, Is.Not.Null);
            Assert.That(worker.Id, Is.EqualTo(byNameWorker.Id));
            Assert.That(testEntity.Name, Is.EqualTo(byNameWorker.Name));
            Assert.That(testEntity.Type, Is.EqualTo(byNameWorker.Type));
            Assert.That(testEntity.Tax, Is.EqualTo(byNameWorker.Tax));
            Assert.That(testEntity.Salary, Is.EqualTo(byNameWorker.Salary));
        }
    }
}