namespace Domain.Types
{
    public enum WorkerType
    {
        Hourly = 1,
        Monthly = 2
    }
}