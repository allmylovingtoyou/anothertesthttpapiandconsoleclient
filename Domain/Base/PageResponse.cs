using System.Collections.Generic;

namespace Domain.Base
{
    public class Page<T>
    {
        public List<T> Data { get; set; }
        public int Total { get; set; }
    }
}