using System;
using Domain.Base;
using Domain.Incomprehensible;
using Domain.Types;

namespace Domain
{
    public class WorkerEntity : BaseEntity
    {
        public string Name { get; set; }
        public WorkerType Type { get; set; }
        public double Tax { get; set; }

        public double Salary
        {
            get
            {
                switch (Type)
                {
                    case WorkerType.Hourly:
                        return new HourlySalaryWorker(this).CalculateMonthSalary();
                    case WorkerType.Monthly:
                        return new MonthSalaryWorker(this).CalculateMonthSalary();
                    default:
                        throw new InvalidProgramException("Incredible enum value");
                }
            }
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }
    }
}