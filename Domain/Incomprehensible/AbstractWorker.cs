namespace Domain.Incomprehensible
{
    public abstract class AbstractWorker : WorkerEntity
    {
        protected AbstractWorker(WorkerEntity entity)
        {
            Tax = entity.Tax;
        }

        public abstract double CalculateMonthSalary();
    }
}