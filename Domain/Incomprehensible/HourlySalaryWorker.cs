﻿namespace Domain.Incomprehensible
{
    public class HourlySalaryWorker : AbstractWorker
    {
        public HourlySalaryWorker(WorkerEntity entity) : base(entity)
        {
        }

        public override double CalculateMonthSalary()
        {
            return 20.8 * 8 * Tax;
        }
    }
}