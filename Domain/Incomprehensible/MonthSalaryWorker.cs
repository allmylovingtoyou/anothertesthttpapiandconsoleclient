﻿namespace Domain.Incomprehensible
{
    public class MonthSalaryWorker : AbstractWorker
    {
        public MonthSalaryWorker(WorkerEntity entity) : base(entity)
        {
        }

        public override double CalculateMonthSalary()
        {
            return Tax;
        }
    }
}