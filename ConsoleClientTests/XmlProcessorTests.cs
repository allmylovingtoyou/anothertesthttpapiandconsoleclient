using System;
using System.Collections.Generic;
using System.IO;
using Api.Dto;
using Api.Requests.Types;
using ConsoleClient.Xml;
using NUnit.Framework;

namespace ConsoleClientTests
{
    public class XmlProcessorTests
    {
        private readonly string _appDir = AppDomain.CurrentDomain.BaseDirectory;
        private XmlProcessor _processor;

        [SetUp]
        public void Setup()
        {
            _processor = new XmlProcessor();
        }

        [Test]
        public void TestDoubleRecords()
        {
            var data = new List<WorkerDto>
            {
                new WorkerDto {Name = "N", Id = 1, Tax = 123, Salary = 15, Type = WorkerTypeDto.Hourly},
                new WorkerDto {Name = "NN", Id = 2, Tax = 123, Salary = 15, Type = WorkerTypeDto.Hourly},
                new WorkerDto {Name = "N", Id = 1, Tax = 123, Salary = 15, Type = WorkerTypeDto.Monthly},
                new WorkerDto {Name = "NN", Id = 2, Tax = 123, Salary = 15, Type = WorkerTypeDto.Monthly}
            };

            var fileCount = GetFileCount();

            _processor.WriteReport(data);

            Assert.That(GetFileCount(), Is.EqualTo(fileCount + 2));
        }

        [Test]
        public void TestEmpty()
        {
            var data = new List<WorkerDto>();
            var fileCount = GetFileCount();
            _processor.WriteReport(data);

            Assert.That(GetFileCount(), Is.EqualTo(fileCount));
        }

        private int GetFileCount()
        {
            return Directory.GetFiles(_appDir + Path.DirectorySeparatorChar + XmlProcessor.SubDir).Length;
        }
    }
}