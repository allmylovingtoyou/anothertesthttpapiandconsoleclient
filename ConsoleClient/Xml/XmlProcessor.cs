using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Api.Dto;
using Api.Requests.Types;

namespace ConsoleClient.Xml
{
    public class XmlProcessor
    {
        public const string SubDir = "XmlReports";
        private readonly string _appDir = AppDomain.CurrentDomain.BaseDirectory;

        public XmlProcessor()
        {
            CheckDirectoryExist(SubDir);
        }

        public void WriteReport(List<WorkerDto> data)
        {
            var hourly = GetByType(data, WorkerTypeDto.Hourly);
            if (hourly.Any()) WriteXml(hourly, WorkerTypeDto.Hourly);

            var monthly = GetByType(data, WorkerTypeDto.Monthly);
            if (monthly.Any()) WriteXml(monthly, WorkerTypeDto.Monthly);
        }

        private List<WorkerDto> GetByType(List<WorkerDto> data, WorkerTypeDto type)
        {
            var result = data.Where(x => x.Type.Equals(type))
                .OrderByDescending(x => x.Tax)
                .ToList();

            return result.Count > 3
                ? result.GetRange(0, 3)
                : result;
        }

        private void WriteXml(List<WorkerDto> data, WorkerTypeDto type)
        {
            var writer = new XmlSerializer(typeof(List<WorkerDto>));

            var now = DateTime.Now;
            var path = _appDir + Path.DirectorySeparatorChar + SubDir + Path.DirectorySeparatorChar
                       + type + "_" + now.Day + "_" + now.Millisecond + ".xml";
            var file = File.Create(path);
            writer.Serialize(file, data);
            file.Close();
        }

        private void CheckDirectoryExist(string path)
        {
            if (Directory.Exists(path)) return;

            Directory.CreateDirectory(_appDir + Path.DirectorySeparatorChar + SubDir);
        }
    }
}