using System;

namespace ConsoleClient.Tools
{
    public class Cmd
    {
        public static string InputString(string colourString)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(colourString);
            Console.ResetColor();
            return Console.ReadLine();
        }
    }
}