using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;

namespace ConsoleClient.Tools
{
    public class HttpTransport
    {
        private const string ContentType = "application/json";
        private readonly IRestClient _rest;

        public HttpTransport(string url)
        {
            var fullUrl = "http://" + url;
            _rest = new RestClient(fullUrl);
        }

        public T Get<T>(string paramsString, string path) where T : new()
        {
            var fullPath = path + "/" + paramsString;
            var request = new RestRequest(fullPath);
            var result = _rest.Execute<T>(request);
            CheckResponse(result);
            return JsonConvert.DeserializeObject<T>(result.Content);
        }

        public TR Post<T, TR>(T requestData, string path) where TR : new()
        {
            var request = new RestRequest(path, Method.POST);
            AddHeaders(request);
            AddRequestData(request, JsonConvert.SerializeObject(requestData));
            var result = _rest.Execute(request);
            CheckResponse(result);

            return JsonConvert.DeserializeObject<TR>(result.Content);
        }

        public TR Put<T, TR>(T requestData, string path) where TR : new()
        {
            var request = new RestRequest(path, Method.PUT);
            AddHeaders(request);
            AddRequestData(request, JsonConvert.SerializeObject(requestData));
            var result = _rest.Execute(request);
            CheckResponse(result);

            return JsonConvert.DeserializeObject<TR>(result.Content);
        }

        private static void CheckResponse(IRestResponse result)
        {
            if (result.StatusCode != HttpStatusCode.OK) throw new InvalidProgramException($"{result.ErrorMessage}");
        }

        private static void AddRequestData(IRestRequest request, string json)
        {
            request.AddParameter(ContentType, json, ParameterType.RequestBody);
        }

        private static void AddHeaders(IRestRequest request)
        {
            request.AddHeader("Content-Type", ContentType);
        }
    }
}