using System;
using System.Threading;
using Api.Dto;
using ConsoleClient.Tools;
using static System.Console;

namespace ConsoleClient.Components
{
    public class Name
    {
        private readonly HttpTransport _transport;

        public Name(HttpTransport transport)
        {
            _transport = transport;
        }

        public void ByNameLoop()
        {
            const string byNameParam = "?name=";
            Clear();

            while (true)
            {
                var actionString = Cmd.InputString("Enter Name or 3 to exit: ");
                WriteLine("");

                if (actionString.Equals("3")) return;

                try
                {
                    var result = _transport.Get<WorkerDto>(byNameParam + actionString, Program.ByNamePath);
                    Clear();
                    WriteLine(result.Id + " " + result.Name + " " + result.Salary);
                    WriteLine();
                }
                catch (Exception)
                {
                    WriteLine($"User by name {actionString} not found");
                    Thread.Sleep(1000);
                    Clear();
                }
            }
        }
    }
}