using System;
using System.Threading;
using Api.Dto;
using Api.Requests;
using Api.Requests.Types;
using ConsoleClient.Tools;
using static System.Console;

namespace ConsoleClient.Components
{
    public class Create
    {
        private readonly HttpTransport _transport;

        public Create(HttpTransport transport)
        {
            _transport = transport;
        }

        public void CreateLoop()
        {
            while (true)
            {
                Clear();
                var name = Cmd.InputString("Input Name: ");
                WriteLine();
                var workerTypeString = Cmd.InputString(
                    $"Input type, string or int value ({(int) WorkerTypeDto.Hourly} - {WorkerTypeDto.Hourly.ToString()} or "
                    + $"{(int) WorkerTypeDto.Monthly} - {WorkerTypeDto.Monthly.ToString()}): ");
                WriteLine();
                var taxString = Cmd.InputString("Input Tax: ");
                WriteLine();


                if (!Enum.TryParse<WorkerTypeDto>(workerTypeString, out var workerType))
                {
                    WriteLine($"Incorrect type: {workerTypeString}");
                    Thread.Sleep(1000);
                    Clear();
                    continue;
                }

                if (!double.TryParse(taxString, out var tax))
                {
                    WriteLine($"Incorrect tax: {taxString}");
                    Thread.Sleep(1000);
                    Clear();
                    continue;
                }

                var request = new CreateRequest
                {
                    Name = name,
                    Type = workerType,
                    Tax = tax
                };

                try
                {
                    _transport.Put<CreateRequest, WorkerDto>(request, Program.CreatePath);
                    WriteLine("Worker created");
                    WriteLine();
                }
                catch (Exception e)
                {
                    WriteLine($"Error on creating: {e.Message}");
                    WriteLine();
                }

                WriteLine("1 - Create");
                WriteLine("2 - Exit");
                WriteLine();
                var inputString = Cmd.InputString("Input 1 to create, 2 to exit: ");

                if (inputString.Equals("2")) return;
            }
        }
    }
}