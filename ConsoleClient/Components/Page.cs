using System;
using Api.Dto;
using Api.Requests;
using Api.Responses;
using ConsoleClient.Tools;
using ConsoleClient.Xml;
using static System.Console;

namespace ConsoleClient.Components
{
    public class Page
    {
        private const int PageSize = 6;
        private readonly HttpTransport _transport;

        public Page(HttpTransport transport)
        {
            _transport = transport;
        }

        public void PageLoop()
        {
            var skip = 0;
            var limit = PageSize;
            var page = GetPageData(skip, limit);

            new XmlProcessor().WriteReport(page.Data);

            var total = page.Total;
            while (true)
            {
                if (skip != 0) WriteLine("1 - Prevous page");

                if (limit < total) WriteLine("2 - Next page");

                WriteLine("3 - Exit");
                WriteLine("");

                var actionString = Cmd.InputString("Input action number: ");

                switch (actionString)
                {
                    case "1":
                        skip -= PageSize;
                        limit -= PageSize;
                        total = GetPageData(skip, limit).Total;
                        break;
                    case "2":
                        skip += PageSize;
                        limit += PageSize;
                        total = GetPageData(skip, limit).Total;
                        break;
                    case "3":
                        return;
                    default:
                        throw new ArgumentException("Illegal argument");
                }
            }
        }

        private PageResponse<WorkerDto> GetPageData(int skip, int limit)
        {
            Clear();
            var request = new PageRequest {Skip = skip, Limit = limit};
            var data = _transport.Post<PageRequest, PageResponse<WorkerDto>>(request, Program.PagePath);
            foreach (var item in data.Data) WriteLine(item.Id + " " + item.Name + " " + item.Salary);

            WriteLine();
            WriteLine($"Total = {data.Total}");
            WriteLine();

            return data;
        }
    }
}