using Api.Responses;
using ConsoleClient.Tools;
using static System.Console;

namespace ConsoleClient.Components
{
    public class Total
    {
        private readonly HttpTransport _transport;

        public Total(HttpTransport transport)
        {
            _transport = transport;
        }

        public void Get()
        {
            Clear();
            var result = _transport.Get<TotalResponse>(null, Program.TotalPath);

            WriteLine($"Total = {result.Total}");
            WriteLine("");
            Cmd.InputString("Press any key to Exit");
        }
    }
}