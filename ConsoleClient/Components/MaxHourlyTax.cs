using Api.Dto;
using ConsoleClient.Tools;
using static System.Console;

namespace ConsoleClient.Components
{
    public class MaxHourlyTax
    {
        private readonly HttpTransport _transport;

        public MaxHourlyTax(HttpTransport transport)
        {
            _transport = transport;
        }

        public void Get()
        {
            Clear();
            var result = _transport.Get<WorkerDto>(null, Program.ByMaxHourlyTaxPath);

            WriteLine($"Id {result.Id}");
            WriteLine($"Name {result.Name}");
            WriteLine($"Tax {result.Tax}");
            WriteLine($"Salary {result.Salary}");
            WriteLine("");
            Cmd.InputString("Press any key to Exit");
        }
    }
}