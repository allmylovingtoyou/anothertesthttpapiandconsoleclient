﻿using System;
using System.Reflection;
using System.Threading;
using ConsoleClient.Components;
using ConsoleClient.Tools;
using static System.Console;

namespace ConsoleClient
{
    internal class Program
    {
        private const string BasePath = "/api/Worker/";
        public const string PagePath = BasePath + "page";
        public const string ByNamePath = BasePath + "byName";
        public const string TotalPath = BasePath + "total";
        public const string ByMaxHourlyTaxPath = BasePath + "byMaxHourlyTax";
        public const string CreatePath = BasePath + "Worker";

        private static HttpTransport _http;

        private static void Main()
        {
            while (true)
                try
                {
                    if (MainLoop()) break;
                }
                catch (Exception e)
                {
                    WriteLine($"Error in program: {e.Message}. Restart");
                    Thread.Sleep(1000);
                }

            WriteLine();
            WriteLine("Good bye");
            Thread.Sleep(1000);
        }

        private static bool MainLoop()
        {
            Clear();
            WriteLine($"Hello to {nameof(ConsoleClient)} version {Assembly.GetEntryAssembly()?.GetName().Version}");

            var url = Cmd.InputString("Enter address and port (192.168.1.1:7896) or enter for default: ");

            if (string.IsNullOrWhiteSpace(url))
            {
                url = "127.0.0.1:5000";
                WriteLine("Set default: 127.0.0.1:5000");
                WriteLine("");
            }

            _http = new HttpTransport(url);


            while (true)
            {
                WriteLine("1 - Get page");
                WriteLine("2 - Get by name");
                WriteLine("3 - Get total");
                WriteLine("4 - Get by maxHourlyTax");
                WriteLine("5 - Add worker");
                WriteLine("6 - Exit");
                WriteLine("");
                var actionString = Cmd.InputString("Input action number: ");

                switch (actionString)
                {
                    case "1":
                        new Page(_http).PageLoop();
                        break;
                    case "2":
                        new Name(_http).ByNameLoop();
                        break;
                    case "3":
                        new Total(_http).Get();
                        break;
                    case "4":
                        new MaxHourlyTax(_http).Get();
                        break;
                    case "5":
                        new Create(_http).CreateLoop();
                        break;
                    case "6":
                        return true;
                    default:
                        throw new ArgumentException("Illegal argument");
                }

                Clear();
            }
        }
    }
}