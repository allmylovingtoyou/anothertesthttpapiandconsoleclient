﻿using System.Threading.Tasks;
using Api.Requests;
using Microsoft.AspNetCore.Mvc;
using TestHttpApiMSSQLConsoleClientXml.Services;

namespace TestHttpApiMSSQLConsoleClientXml.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WorkerController : Controller
    {
        private readonly WorkerService _service;

        public WorkerController(WorkerService service)
        {
            _service = service;
        }

        [HttpPut]
        public async Task<JsonResult> Worker([FromBody] CreateRequest request)
        {
            var result = await _service.Create(request);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> Page([FromBody] PageRequest request)
        {
            var result = await _service.GetPage(request);
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> ByName(string name)
        {
            var result = await _service.ByName(name);

            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> Total(string name)
        {
            var result = await _service.TotalMonthSalary();

            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> ByMaxHourlyTax(string name)
        {
            var result = await _service.ByMaxHourlyTax();

            return Json(result);
        }
    }
}