using Api.Dto;
using Api.Requests;
using AutoMapper;
using Domain;
using TestHttpApiMSSQLConsoleClientXml.Mappings.Base;

namespace TestHttpApiMSSQLConsoleClientXml.Mappings
{
    public class WorkerMapper : AbstractBaseMapper<WorkerEntity, WorkerDto>
    {
        private readonly IMapper _mapper;

        public WorkerMapper(IMapper mapper) : base(mapper)
        {
            _mapper = mapper;
        }

        public WorkerEntity ToEntity(CreateRequest dto)
        {
            return _mapper.Map<WorkerEntity>(dto);
        }
    }
}