using System.Collections.Generic;
using System.Linq;
using Api.Responses;
using AutoMapper;
using Domain.Base;

namespace TestHttpApiMSSQLConsoleClientXml.Mappings.Base
{
    public abstract class AbstractBaseMapper<TE, TD>
    {
        private readonly IMapper _mapper;

        public AbstractBaseMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public TD ToDto(TE dto)
        {
            return _mapper.Map<TD>(dto);
        }

        public TE ToEntity(TD dto)
        {
            return _mapper.Map<TE>(dto);
        }

        public List<TD> ToDto(IEnumerable<TE> entities)
        {
            return entities.Select(ToDto)
                .ToList();
        }

        public List<TE> ToEntity(IEnumerable<TD> dtos)
        {
            return dtos.Select(ToEntity)
                .ToList();
        }

        public PageResponse<TD> ToDto(Page<TE> entity)
        {
            return new PageResponse<TD>
            {
                Data = ToDto(entity.Data),
                Total = entity.Total
            };
        }
    }
}