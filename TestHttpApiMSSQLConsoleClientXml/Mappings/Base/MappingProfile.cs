using Api.Dto;
using Api.Requests;
using AutoMapper;
using Domain;

namespace TestHttpApiMSSQLConsoleClientXml.Mappings.Base
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<WorkerEntity, WorkerDto>()
                .ForMember(m => m.XmlId, opt => opt.Ignore());

            CreateMap<CreateRequest, WorkerEntity>()
                .ForMember(m => m.Type, opt => opt.MapFrom(s => s.Type))
                .ForMember(m => m.Salary, opt => opt.Ignore())
                .ForMember(m => m.Id, opt => opt.Ignore());
        }
    }
}