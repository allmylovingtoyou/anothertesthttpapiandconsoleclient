using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Db;
using Domain;
using Domain.Base;
using Domain.Types;
using Microsoft.EntityFrameworkCore;
using TestHttpApiMSSQLConsoleClientXml.Repositories.Base;
using TestHttpApiMSSQLConsoleClientXml.Utils;

namespace TestHttpApiMSSQLConsoleClientXml.Repositories
{
    public class WorkerRepository : AbstractRepository<WorkerEntity>
    {
        private readonly CultureInfo _culture = CultureInfo.InvariantCulture;

        private readonly string _specifier = "G";

        public WorkerRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<WorkerEntity> ByName(string name)
        {
            var result = DbContext.WorkerEntities.FromSql($"ByName @Name = {name}")
                .FirstOrDefault();

            if (result == null) throw new InternalExceptions.NotFoundException($"{nameof(WorkerEntity)} by name {name}");

            return Task.FromResult(result);
        }

        public override Task<WorkerEntity> Add(WorkerEntity entity)
        {
            //TODO need refactor
            var result = DbContext.WorkerEntities.FromSql("DECLARE @return_value int " +
                                                          "EXEC @return_value = AddWorker " +
                                                          $"@Name = '{entity.Name}', " +
                                                          $"@Type = {(int) entity.Type}, " +
                                                          $"@Tax = {entity.Tax.ToString(_specifier, _culture)} ," +
                                                          $"@Salary = {entity.Salary.ToString(_specifier, _culture)} " +
                                                          "Select * from WorkerEntities where id = (SELECT	'Return Value' = @return_value)");

            if (result == null) throw new InvalidProgramException("Can't add entity");

            return ById(result.First().Id);
        }

        public Task<Page<WorkerEntity>> PageHardCodeSort(int skip, int limit)
        {
            var dbResult = DbContext.Set<WorkerEntity>()
                .AsQueryable()
                .OrderByDescending(x => x.Salary)
                .ThenBy(x => x.Name);

            var total = dbResult.Count();
            var data = dbResult.Skip(skip).Take(limit).ToList();

            var result = new Page<WorkerEntity>
            {
                Data = data,
                Total = total
            };

            return Task.FromResult(result);
        }

        public Task<List<double>> AllSalaries()
        {
            return Task.FromResult(DbContext.WorkerEntities
                .Select(x => x.Salary)
                .ToList());
        }

        public Task<WorkerEntity> ByMaxHourlyTax()
        {
            return Task.FromResult(DbContext.WorkerEntities
                .Where(w => w.Type.Equals(WorkerType.Hourly))
                .OrderByDescending(w => w.Tax)
                .FirstOrDefault());
        }
    }
}