using System.Linq;
using System.Threading.Tasks;
using Db;
using Domain.Base;
using Microsoft.EntityFrameworkCore;
using TestHttpApiMSSQLConsoleClientXml.Utils;

namespace TestHttpApiMSSQLConsoleClientXml.Repositories.Base
{
    public abstract class AbstractRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationDbContext DbContext;

        protected AbstractRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual async Task<T> Add(T entity)
        {
            var result = await DbContext.Set<T>().AddAsync(entity);
            await DbContext.SaveChangesAsync();

            return result.Entity;
        }

        public virtual async Task<T> ById(int id)
        {
            var entity = await ByIdOrThrow(id, DbContext);
            return entity;
        }

        public virtual async Task<T> Delete(int id)
        {
            var entity = await ByIdOrThrow(id, DbContext);
            DbContext.Set<T>().Remove(entity);
            return entity;
        }

        public virtual Task<Page<T>> Page(int skip, int limit)
        {
            var dbResult = DbContext.Set<T>().AsQueryable();

            var total = dbResult.Count();
            var data = dbResult.Skip(skip).Take(limit).ToList();

            var result = new Page<T>
            {
                Data = data,
                Total = total
            };

            return Task.FromResult(result);
        }

        private static async Task<T> ByIdOrThrow(int id, DbContext db)
        {
            var result = await db.Set<T>().FindAsync(id);
            if (result == null) throw new InternalExceptions.NotFoundException(id.ToString());

            return result;
        }
    }
}