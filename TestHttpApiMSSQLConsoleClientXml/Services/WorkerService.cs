using System.Linq;
using System.Threading.Tasks;
using Api.Dto;
using Api.Requests;
using Api.Responses;
using TestHttpApiMSSQLConsoleClientXml.Mappings;
using TestHttpApiMSSQLConsoleClientXml.Repositories;

namespace TestHttpApiMSSQLConsoleClientXml.Services
{
    public class WorkerService
    {
        private readonly WorkerMapper _mapper;
        private readonly WorkerRepository _repository;

        public WorkerService(WorkerRepository repository, WorkerMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<WorkerDto> Create(CreateRequest request)
        {
            var entity = _mapper.ToEntity(request);
            var result = await _repository.Add(entity);

            return _mapper.ToDto(result);
        }

        public async Task<WorkerDto> ByName(string name)
        {
            var result = await _repository.ByName(name);

            return _mapper.ToDto(result);
        }

        public async Task<PageResponse<WorkerDto>> GetPage(PageRequest request)
        {
            var page = await _repository.PageHardCodeSort(request.Skip, request.Limit);
            return _mapper.ToDto(page);
        }

        public async Task<TotalResponse> TotalMonthSalary()
        {
            return new TotalResponse
            {
                Total = (await _repository.AllSalaries())
                    .Sum(x => x)
            };
        }

        public async Task<WorkerDto> ByMaxHourlyTax()
        {
            var result = await _repository.ByMaxHourlyTax();
            return _mapper.ToDto(result);
        }
    }
}