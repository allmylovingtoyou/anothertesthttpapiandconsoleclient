﻿using AutoMapper;
using Db;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestHttpApiMSSQLConsoleClientXml.Mappings;
using TestHttpApiMSSQLConsoleClientXml.Mappings.Base;
using TestHttpApiMSSQLConsoleClientXml.Repositories;
using TestHttpApiMSSQLConsoleClientXml.Services;
using TestHttpApiMSSQLConsoleClientXml.Utils;
using static TestHttpApiMSSQLConsoleClientXml.Utils.Constants;

namespace TestHttpApiMSSQLConsoleClientXml
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var autoMapper = CreateAutoMapper();
            services.AddSingleton(autoMapper);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(ConfigConnectionString)));

            services.AddScoped<WorkerMapper>();
            services.AddScoped<WorkerRepository>();
            services.AddScoped<WorkerService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(builder => builder.AllowAnyOrigin());
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseHttpsRedirection();
            app.UseMvc();
        }

        private static IMapper CreateAutoMapper()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); });
            var autoMapper = mapperConfiguration.CreateMapper();
            autoMapper.ConfigurationProvider.AssertConfigurationIsValid();
            return autoMapper;
        }
    }
}