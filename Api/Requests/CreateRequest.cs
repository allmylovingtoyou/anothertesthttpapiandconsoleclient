using Api.Requests.Types;

namespace Api.Requests
{
    public class CreateRequest
    {
        public string Name { get; set; }
        public double Tax { get; set; }
        public WorkerTypeDto Type { get; set; }
    }
}