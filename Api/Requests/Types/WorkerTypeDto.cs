namespace Api.Requests.Types
{
    public enum WorkerTypeDto
    {
        Hourly = 1,
        Monthly = 2
    }
}