namespace Api.Requests
{
    public class PageRequest
    {
        public PageRequest()
        {
        }

        public PageRequest(int skip, int limit)
        {
            Skip = skip;
            Limit = limit;
        }

        public int Skip { get; set; }
        public int Limit { get; set; }
    }
}