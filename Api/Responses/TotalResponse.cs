namespace Api.Responses
{
    public class TotalResponse
    {
        public double Total { get; set; }
    }
}