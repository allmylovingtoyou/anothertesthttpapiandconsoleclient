using System.Xml.Serialization;
using Api.Requests.Types;
using Newtonsoft.Json;

namespace Api.Dto
{
    [XmlRoot(ElementName = nameof(WorkerDto))]
    public class WorkerDto
    {
        [XmlIgnore]
        public int? Id { get; set; }

        [XmlAttribute(AttributeName = nameof(Name))]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = nameof(Tax))]
        public double Tax { get; set; }

        [XmlIgnore]
        public WorkerTypeDto Type { get; set; }

        [XmlAttribute(AttributeName = nameof(Salary))]
        public double Salary { get; set; }

        [JsonIgnore]
        [XmlAttribute(AttributeName = nameof(Id))]
        public int XmlId
        {
            get => Id ?? 0;
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }
    }
}