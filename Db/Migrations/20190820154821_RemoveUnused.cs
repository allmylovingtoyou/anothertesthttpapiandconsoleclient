﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Db.Migrations
{
    public partial class RemoveUnused : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "MiddleName",
                "WorkerEntities");

            migrationBuilder.DropColumn(
                "Surname",
                "WorkerEntities");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                "MiddleName",
                "WorkerEntities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                "Surname",
                "WorkerEntities",
                nullable: true);
        }
    }
}