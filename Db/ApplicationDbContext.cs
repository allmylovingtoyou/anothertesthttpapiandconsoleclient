﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Db
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<WorkerEntity> WorkerEntities { get; set; }

        /// <summary>
        ///     For migrations and develop
        /// </summary>
        /// <param name="options"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
                options.UseSqlServer("Server=RAB-04\\SQLEXPRESS1;Database=Tn_Test_Db;Trusted_Connection=True;ConnectRetryCount=0;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkerEntity>()
                .HasIndex(p => new {p.Name}).IsUnique();
        }
    }
}